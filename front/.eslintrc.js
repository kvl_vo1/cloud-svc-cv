module.exports = {
  'extends': 'airbnb',
  'rules': {
    "prefer-template": "off",
    "quotes": [2, "double" ],
    "no-console": "off",
    'linebreak-style': [2, 'windows'],
    'indent': [2, 2],
    'spaced-comment': [2, 'never'],
    'one-var': [0]
  },
  "env": {
    "browser": true,
    "node": true,
    "jquery": true,
  },
};

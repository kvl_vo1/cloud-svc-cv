package web

import (
  "bytes"
  "compress/gzip"
  "encoding/base64"
  "encoding/json"
  "fmt"
  "io/ioutil"
  "mime"
  "net/http"
  "os"
  "path/filepath"
  "strings"
  "time"

  "gopkg.in/mgo.v2/bson"

  a "cloud-svc-cv/arch"
  m "cloud-svc-cv/mongo"
  t "cloud-svc-cv/types"
  u "cloud-svc-cv/utils"
)

type WebServer struct {
  fs, sf   http.Handler
  mux      *http.ServeMux
  settings *t.Settings
}

func (ws *WebServer) Init(s *t.Settings) {
  ws.fs = http.FileServer(http.Dir("./public" /*"./front/public"*/))
  ws.sf = http.StripPrefix("/tools/compression", ws.fs)
  ws.settings = s

  _ = mime.AddExtensionType(".css", "text/css; charset=utf-8")
  _ = mime.AddExtensionType(".gif", "image/gif")
  _ = mime.AddExtensionType(".htm", "text/html; charset=utf-8")
  _ = mime.AddExtensionType(".html", "text/html; charset=utf-8")
  _ = mime.AddExtensionType(".jpg", "image/jpeg")
  _ = mime.AddExtensionType(".js", "application/x-javascript")
  _ = mime.AddExtensionType(".pdf", "application/pdf")
  _ = mime.AddExtensionType(".png", "image/png")
  _ = mime.AddExtensionType(".svg", "image/svg+xml")
  _ = mime.AddExtensionType(".xml", "text/xml; charset=utf-8")

  ws.mux = http.NewServeMux()

  ws.mux.HandleFunc("/list", ws.listHandler)
  ws.mux.HandleFunc("/file", ws.fileHandler)
  ws.mux.HandleFunc("/drop", ws.dropHandler)
  ws.mux.HandleFunc("/setinfo", ws.setInfoHandler)
  ws.mux.HandleFunc("/tools/compression/", ws.toolsCompression)
}

func (ws *WebServer) Run() error {
  if err := http.ListenAndServeTLS(":9000", "server.pem", "server.key", ws.mux); err != nil {
    return err
  }
  return nil
}

func (ws *WebServer) listHandler(res http.ResponseWriter, req *http.Request) {
  ms := ws.settings.Mongo.Session.Copy()
  defer ms.Close()

  var procList []string
  var q bson.M

  body, err := ioutil.ReadAll(req.Body)
  if err != nil {
    e := u.MakeError("listHandler.ReadAll", err.Error())
    fmt.Println(e)
    res.WriteHeader(http.StatusInternalServerError)
    _, _ = res.Write([]byte(e.Error()))
    return
  }

  if len(body) > 0 {
    if e := json.Unmarshal(body, &procList); e != nil {
      e = u.MakeError("listHandler.Unmarshal(body)", e.Error())
      fmt.Println(e)
      res.WriteHeader(http.StatusInternalServerError)
      _, _ = res.Write([]byte(e.Error()))
      return
    }
  }

  var filesList []t.FileItem
  var tmpResult []t.FileItem

  var c = ms.DB(ws.settings.Mongo.Database).C(ws.settings.Mongo.Collections.Workfiles)
  var lc = 100
  var s = bson.M{"_id": 1, "mws.fileName": 1}

  if len(procList) > 0 {
    // цикл по всіх заданих проектах
    for _, proc := range procList {
      q = bson.M{"htag": string(proc)}
      if e := c.Find(q).Limit(lc).Select(s).All(&tmpResult); e != nil {
        break
      }

      filesList = append(filesList, tmpResult...)
      if len(filesList) >= (lc * 5) {
        break
      }
    }
  } else {
    // забір перших "lc" файлів
    err = c.Find(q).Limit(lc).Select(s).All(&filesList)
  }

  if err != nil {
    e := u.MakeError("listHandler.MongoSelect", err.Error())
    fmt.Println(e)
    res.WriteHeader(http.StatusInternalServerError)
    _, _ = res.Write([]byte(e.Error()))
    return
  } else {
    res.Header().Set("Content-Type", "application/json; charset=utf-8")
    res.WriteHeader(http.StatusOK)
    if len(filesList) > 0 {
      for i := range filesList {
        filesList[i].FileName = filesList[i].Mws.Filename
        filesList[i].Mws = nil
      }

      b, _ := json.Marshal(filesList)
      _, _ = res.Write(b)
    }
  }
}

func (ws *WebServer) fileHandler(res http.ResponseWriter, req *http.Request) {
  ms := ws.settings.Mongo.Session.Copy()
  defer ms.Close()

  body, err := ioutil.ReadAll(req.Body)
  if err != nil {
    e := u.MakeError("FileHandler.ReadAll", err.Error())
    fmt.Println(e)
    res.WriteHeader(http.StatusInternalServerError)
    _, _ = res.Write([]byte(e.Error()))
    return
  } else {
    var fileItem t.FileItem
    if e := json.Unmarshal(body, &fileItem); e != nil {
      fmt.Println(e)
      res.WriteHeader(http.StatusInternalServerError)
      _, _ = res.Write([]byte(e.Error()))
      return
    }

    if &(fileItem.ID) != nil {
      c := ms.DB(ws.settings.Mongo.Database).C(ws.settings.Mongo.Collections.Workfiles)
      q := bson.M{"_id": fileItem.ID} // id документа для видалення
      var meta t.JsonMeta

      if e := c.Find(q).One(&meta); e != nil {
        fmt.Println(e)
        res.WriteHeader(http.StatusInternalServerError)
        _, _ = res.Write([]byte(e.Error()))
        return
      } else {
        fn := filepath.Join(ws.settings.General.WorkDir, meta.MWS.FileName)
        b, e1 := ioutil.ReadFile(fn)
        if e1 != nil {
          fmt.Println(e1)
          res.WriteHeader(http.StatusInternalServerError)
          _, _ = res.Write([]byte(e1.Error()))

          // видалення документа з оперативної колекції, коли файл фізично відсутній
          e1 = c.Remove(q)
          if e1 != nil {
            fmt.Println(u.MakeError("FileHandler:c.Remove", e1.Error()))
          } else {
            fmt.Println(fmt.Sprintf("Документ '%s'(файл '%s') видалено.", fileItem.ID.Hex(), fileItem.FileName))
          }
        } else {
          meta.MWS.ID = fileItem.ID
          res.Header().Set("Content-Type", "application/json; charset=utf-8")
          res.Header().Set("Content-Encoding", "gzip")
          res.Header().Set("X-Mongo-Id", fileItem.ID.Hex())

          bm, e2 := json.Marshal(meta)
          if e2 != nil {
            fmt.Println(e2)
          } else {
            res.Header().Set("Content-Meta", u.Base64Encode(string(bm)))
            res.WriteHeader(http.StatusOK)
            _, _ = res.Write(b)
          }
        }
      }
    }
  }
}

func (ws *WebServer) dropHandler(res http.ResponseWriter, req *http.Request) {
  ms := ws.settings.Mongo.Session.Copy()
  defer ms.Close()

  body, err := ioutil.ReadAll(req.Body)
  if err != nil {
    e := u.MakeError("DropHandler.ReadAll", err.Error())
    fmt.Println(e)
    res.WriteHeader(http.StatusInternalServerError)
    _, _ = res.Write([]byte(e.Error()))
    return
  } else {
    var filesList []t.FileItem

    err = json.Unmarshal(body, &filesList)
    if err != nil {
      e := u.MakeError("json.Unmarshal(body)", err.Error())
      fmt.Println(e)
      res.WriteHeader(http.StatusInternalServerError)
      _, _ = res.Write([]byte(e.Error()))
      return
    } else {
      if len(filesList) > 0 {
        for i := range filesList {
          err = m.MoveDocument(ms, ws.settings.Mongo.Database, ws.settings.Mongo.Collections.Workfiles, ws.settings.Mongo.Collections.History, filesList[i].ID)

          if err != nil {
            e := u.MakeError("MoveDocument", err.Error())
            fmt.Println(e)
          } else {
            // фізичне видалення файлу
            e := os.Remove(filepath.Join(ws.settings.General.WorkDir, filesList[i].FileName))
            if e != nil {
              fmt.Println(u.MakeError("os.Remove %filename%", e.Error()))
            }
          }
        }
      }

      res.Header().Set("Content-Type", "application/json; charset=utf-8")
      res.WriteHeader(http.StatusOK)
    }
  }
}

func (ws *WebServer) setInfoHandler(res http.ResponseWriter, req *http.Request) {
  ms := ws.settings.Mongo.Session.Copy()
  defer ms.Close()

  body, err := ioutil.ReadAll(req.Body)
  if err != nil {
    e := u.MakeError("SetInfoHandler.ReadAll", err.Error())
    fmt.Println(e)
    res.WriteHeader(http.StatusInternalServerError)
    _, _ = res.Write([]byte(e.Error()))
  } else {
    isGzipped := strings.Contains(req.Header.Get("Content-Encoding"), "gzip")
    if isGzipped {
      b := bytes.NewReader(body)
      fz, e := gzip.NewReader(b)
      if e != nil {
        fmt.Println(e)
        res.WriteHeader(http.StatusInternalServerError)
        _, _ = res.Write([]byte(e.Error()))
        return
      }
      defer fz.Close()

      body, e = ioutil.ReadAll(fz)
      if e != nil {
        fmt.Println(e)
        res.WriteHeader(http.StatusInternalServerError)
        _, _ = res.Write([]byte(e.Error()))
        return
      }
    }

    var filesList []t.MWS
    err = json.Unmarshal(body, &filesList)
    if err != nil {
      fmt.Println(err)
      res.WriteHeader(http.StatusInternalServerError)
      _, _ = res.Write([]byte(err.Error()))
      return
    }

    c := ms.DB(ws.settings.Mongo.Database).C(ws.settings.Mongo.Collections.History)
    for i := range filesList {
      q := bson.M{"_id": filesList[i].ID} // id документа
      change := bson.M{
        "$set": bson.M{
          "mws.receiveID":      filesList[i].ReceiveID,
          "mws.brNick":         filesList[i].BrNick,
          "mws.orgId":          filesList[i].OrgId,
          "mws.orgName":        filesList[i].OrgName,
          "mws.corpId":         filesList[i].CorpId,
          "mws.corpName":       filesList[i].CorpName,
          "mws.message":        filesList[i].Message,
          "mws.isWarning":      filesList[i].IsWarning,
          "mws.isError":        filesList[i].IsError,
          "mws.items.new":      filesList[i].Items.New,
          "mws.items.unlinked": filesList[i].Items.Unlinked,
          "mws.lastUpdate":     time.Now().Unix()}}

      err = c.Update(q, change)
      if err != nil {
        fmt.Println(err)
        res.WriteHeader(http.StatusInternalServerError)
        _, _ = res.Write([]byte(err.Error()))
        return
      }

      // отримую дані по геоаптечних проектах
      if ws.settings.General.CollectGSdata && (filesList[i].HTag == "geoapt.ua" || filesList[i].HTag == "geoapt.ru") {
        var m t.JsonMeta
        if e := c.Find(q).One(&m); e != nil {
          fmt.Println("Search error: ", e)
          res.WriteHeader(http.StatusInternalServerError)
          _, _ = res.Write([]byte(e.Error()))
          return
        }

        // вставка модифікованих даних для відображення в QV
        var qj t.QVLog
        qj.ID = filesList[i].ID
        qj.HTag = m.HTag
        qj.Unix = m.Unix
        qj.Auth = m.Auth
        qj.Link.ID = m.Link.ID
        qj.Link.IDAddr = m.Link.IDAddr
        qj.Link.IDOrgn = m.Link.IDOrgn
        qj.Link.IDLink = m.Link.IDLink
        qj.Link.EGRPOU = m.Link.EGRPOU
        qj.Link.OrgName = fmt.Sprintf("%s/%s: %s", m.Head, m.Name, m.Addr)

        qj.MWS = m.MWS
        qj.MWS.FileName = ""

        collection := ms.DB(ws.settings.Mongo.Database).C(ws.settings.Mongo.Collections.QV)
        _ = collection.RemoveId(qj.ID)

        err = collection.Insert(qj)
        if err != nil {
          fmt.Println("Inserting error into 'QV': ", err)
          res.WriteHeader(http.StatusInternalServerError)
          _, _ = res.Write([]byte(err.Error()))
          return
        }
      }
    }

    res.Header().Set("Content-Type", "application/json; charset=utf-8")
    res.WriteHeader(http.StatusOK)
  }
}

func (ws *WebServer) toolsCompression(res http.ResponseWriter, req *http.Request) {
  if req.Method == http.MethodGet {
    ws.sf.ServeHTTP(res, req)
    return
  }

  res.Header().Set("Access-Control-Allow-Origin", "*")
  res.Header().Set("Access-Control-Allow-Headers", "Content-Type, Accept, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
  res.Header().Set("Access-Control-Allow-Methods", "HEAD,GET,PUT,POST,DELETE,OPTIONS")

  // Stop here if its Preflighted OPTIONS request
  if req.Method == http.MethodOptions {
    return
  }

  body, err := ioutil.ReadAll(req.Body)
  if err != nil {
    e := u.MakeError("ToolsCompression.ReadAll", err.Error())
    fmt.Println(e)
    res.WriteHeader(http.StatusInternalServerError)
    res.Write([]byte(e.Error()))
    return
  }

  var d struct {
    Operation string `json:"operation"`
    Data      string `json:"data"`
    Result    struct {
      Mime     string `json:"mime,omitempty"`
      Original int64  `json:"original,omitempty"`
      Zipped   int64  `json:"zipped,omitempty"`
      Base64   int64  `json:"base64,omitempty"`
    } `json:"result,omitempty"`
  }

  if err = json.Unmarshal(body, &d); err != nil {
    fmt.Println(err)
    res.WriteHeader(http.StatusInternalServerError)
    _, _ = res.Write([]byte(err.Error()))
    return
  }

  d.Result.Mime = ""
  d.Result.Original = int64(len(d.Data))

  switch d.Operation {
  case "gzip":
    // text -> gzip
    gzd, e := a.GZip([]byte(d.Data))
    if e != nil {
      fmt.Println(e)
      res.WriteHeader(http.StatusInternalServerError)
      _, _ = res.Write([]byte(e.Error()))
      return
    }
    d.Result.Zipped = int64(len(gzd))

    // gzip -> base64
    d.Data = base64.StdEncoding.EncodeToString(gzd)
    d.Result.Base64 = int64(len(d.Data))

  case "ungzip":
    // вирізка MIME
    if strings.HasPrefix(d.Data, "data:") {
      s := strings.SplitAfter(d.Data, ",")
      if len(s) == 2 {
        d.Result.Mime = s[0][:len(s[0])-1]
        d.Data = s[1]
      }
    }

    // base64 -> gzip
    gzd, e := base64.StdEncoding.DecodeString(d.Data)
    if e != nil {
      fmt.Println(e)
      res.WriteHeader(http.StatusInternalServerError)
      res.Write([]byte(e.Error()))
      return
    }

    d.Result.Base64 = int64(len(d.Data))
    d.Result.Zipped = int64(len(gzd))

    // gzip -> text
    gzd, e = a.UnGZip(gzd)
    if e != nil {
      fmt.Println(e)
      res.WriteHeader(http.StatusInternalServerError)
      res.Write([]byte(e.Error()))
      return
    }
    d.Data = string(gzd)
    d.Result.Original = int64(len(d.Data))

  default:
    e := u.MakeError("ToolsCompression", fmt.Sprintf(`unsupported operation: "%s"`, d.Operation))
    fmt.Println(e)
    res.WriteHeader(http.StatusInternalServerError)
    _, _ = res.Write([]byte(e.Error()))
    return
  }

  body, _ = json.Marshal(d)
  res.Header().Set("Content-Type", "application/json; charset=utf-8")
  res.WriteHeader(http.StatusOK)
  _, _ = res.Write([]byte(body))
}

package arch

import (
  "bytes"
  "compress/gzip"
  "io/ioutil"
  "log"
  "os"
)

func GZip(data []byte) ([]byte, error) {
  // gzip даних
  var b bytes.Buffer
  w := gzip.NewWriter(&b)
  if _, err := w.Write(data); err != nil {
    return nil, err
  }

  if err := w.Close(); err != nil {
    return nil, err
  }

  return b.Bytes(), nil
}

func UnGZip(data []byte) ([]byte, error) {
  b := bytes.NewReader(data)
  r, err := gzip.NewReader(b)
  if err != nil {
    return nil, err
  }
  defer func() {
    _ = r.Close()
  }()

  gzd, err := ioutil.ReadAll(r)
  if err != nil {
    log.Println(err)
    return nil, err
  }

  return gzd, nil
}

func GzipAndSaveToFile(fileName string, data []byte) error {
  // gzip даних
  var b bytes.Buffer
  w := gzip.NewWriter(&b)
  if _, err := w.Write(data); err != nil {
    return err
  }

  // You must close this first to flush the bytes to the buffer.
  if err := w.Close(); err != nil {
    return err
  }

  if err := ioutil.WriteFile(fileName, b.Bytes(), os.ModePerm); err != nil {
    return err
  }
  return nil
}

func SaveBytesToFile(FileName string, b *[]byte) error {
  f, err := os.Create(FileName)
  if err != nil {
    return err
  }
  defer func() {
    _ = f.Close()
  }()

  if _, err = f.Write(*b); err != nil {
    return err
  }

  return nil
}

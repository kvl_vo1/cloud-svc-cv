const path = require("path");
const webpack = require("webpack");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const Visualizer = require("webpack-visualizer-plugin");

const SourceDir = path.join(__dirname, "src");
const PublicDir = path.join(__dirname, "public");

const config = {
  devServer: {
    contentBase: PublicDir,
    host: "localhost",
    port: 8000,
    compress: true,
    open: true,
  },

  //entry: path.join(SourceDir, '/mainApp.jsx'),
  entry: {
    //'webpack-dev-server/client?http://localhost:8000',
    //'webpack/hot/dev-server',
    //mainApp: ['webpack-dev-server/client?http://localhost:8000', SourceDir + "mainApp.jsx"]
    mainApp: [SourceDir + "/mainApp.jsx"],
  },

  output: {
    //Tell webpack to include comments in bundles with information about the contained modules.
    //This option defaults to false and should not be used in production, but it's very useful
    //in development when reading the generated code.
    pathinfo: false,
    path: PublicDir,
    publicPath: "",
    filename: "js/[name].js",
  },

  resolve: {
    extensions: ["*", ".js", ".jsx"],
  },

  module: {
    loaders: [
      {
        test: /\.(js|jsx)$/,
        loader: "babel-loader",
        exclude: [/node_modules/, /public/],
        query: { presets: ["es2015", "react"] },
      },
      {
        test: /\.css$/,
        loader: "style-loader!css-loader!autoprefixer-loader",
        exclude: [/node_modules/, /public/],
      },
      {
        test: /\.less$/,
        loader: "style-loader!css-loader!autoprefixer!less-loader",
        exclude: [/node_modules/, /public/],
      },
      {
        test: /\.scss$/,
        exclude: [/node_modules/, /public/],
        loader: ExtractTextPlugin.extract({
          //fallbackLoader: 'style-loader',
          //css?minimize
          loader: ["css-loader", "sass-loader?outputStyle=expanded"], //uncompressed compressed expanded
        }),
      },
      {
        test: /\.gif$/,
        exclude: [/node_modules/, /public/],
        loader: "url-loader?limit=10000&mimetype=image/gif",
      },
      {
        test: /\.jpg$/,
        exclude: [/node_modules/, /public/],
        loader: "url-loader?limit=10000&mimetype=image/jpg",
      },
      {
        test: /\.png$/,
        exclude: [/node_modules/, /public/],
        loader: "url-loader?limit=10000&mimetype=image/png",
      },
      {
        test: /\.svg$/,
        exclude: [/node_modules/, /public/],
        loader: "url-loader?limit=26000&mimetype=image/svg+xml",
      },
      {
        test: /\.json$/,
        exclude: [/node_modules/, /public/],
        loader: "json-loader",
      },
      //{
      //test: /\.html$/,
      //loader: 'raw-loader',
      //},
    ],
  },

  plugins: [
    new HtmlWebpackPlugin({
      title: "GZip [де]компресія",
      jq: "js/jquery/jquery.min.js",
      bs: ["js/bootstrap/css/bootstrap.min.css", "js/bootstrap/js/bootstrap.min.js"],
      ie: ["js/ie/html5shiv.min.js", "js/ie/respond.min.js"],
      //minify: {
      //collapseWhitespace: true
      //},
      //hash: true,
      template: path.join(SourceDir, "index.html"),
    }),

    new CopyWebpackPlugin([
      {
        from: "node_modules/bootstrap/dist/css/*.min.css",
        to: PublicDir + "/js/bootstrap/css/[name].[ext]",
      },
      {
        from: "node_modules/bootstrap/dist/js/*.min.js",
        to: PublicDir + "/js/bootstrap/js/[name].[ext]",
      },
      {
        from: "node_modules/bootstrap/dist/fonts/*.*",
        to: PublicDir + "/js/bootstrap/fonts/[name].[ext]",
      },
      {
        from: "node_modules/jquery/dist/jquery.min.js",
        to: PublicDir + "/js/jquery",
      },
      {
        from: "node_modules/html5shiv/dist/html5shiv.min.js",
        to: PublicDir + "/js/ie",
      },
      {
        from: "node_modules/respond.js/dest/respond.min.js",
        to: PublicDir + "/js/ie",
      },
      {
        from: SourceDir + "/index.html",
        to: PublicDir + "/[name].[ext]",
      },
      {
        from: SourceDir + "/favicon.ico",
        to: PublicDir + "/[name].[ext]",
      },
    ]),

    new ExtractTextPlugin({
      filename: "css/styles.css",
      disable: false,
      allChunks: true,
    }),
  ],

  stats: {
    colors: true,
  },
};

console.log(`*** ${String(process.env.NODE_ENV === undefined ? "n/a" : process.env.NODE_ENV).toUpperCase()} ***`);

if (process.env.NODE_ENV === "dev") {
  config.devtool = "inline-source-map"; //'eval-source-map','eval'
  config.plugins.push(new Visualizer({ filename: "./statistics.html" }));
}

module.exports = config;

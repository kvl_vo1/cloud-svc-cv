package main

import (
  a "cloud-svc-cv/app"
)

func main()  {
  var app a.App
  defer app.Close()

  if err := app.Run(); err != nil {
    panic(err)
  }
}

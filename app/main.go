package app

import (
  m "cloud-svc-cv/mongo"
  n "cloud-svc-cv/nats"
  p "cloud-svc-cv/proc"
  t "cloud-svc-cv/types"
  w "cloud-svc-cv/web"

  "encoding/json"
  "fmt"
  "io/ioutil"
  "os"
  "path/filepath"

  "gopkg.in/mgo.v2"
)

type App struct {
  useNats          bool
  rootDir, stnFile string
  params           t.Settings
  session          *mgo.Session
}

func (app *App) Log() {
  fmt.Println("*END*")
}

func (app *App) Init() error {
  app.useNats = true
  app.rootDir = filepath.Dir(os.Args[0])
  app.stnFile = filepath.Join(app.rootDir, "settings.json")

  app.params.IsActive = false
  app.params.General.CollectGSdata = true
  app.params.General.WorkDir = filepath.Join(app.rootDir, "stream-out")
  app.params.Mongo.Collections = t.Collections{Workfiles: "workfiles", History: "history", QV: "qv"}
  app.params.Mongo.Host = "localhost"
  app.params.Mongo.Database = "mws"
  app.params.Mongo.Login = ""
  app.params.Mongo.Password = ""
  app.params.Countries = []*t.CountryParams{
    {
      Country:   "ua",
      NatsAddr:  "nats://login:password@nats.server.ua:4222",
      PopData:   "https://price.server.ua/stream-pop-data",
      SubsToken: "s1.stream-out",
      IsActive:  true,
    },
    {
      Country:   "ru",
      NatsAddr:  "nats://login:password@nats.server.ru:4222",
      PopData:   "https://price.server.ru/stream-pop-data",
      SubsToken: "s1.stream-out",
      IsActive:  true,
    },
  }
  app.params.OnNATSData = p.ProcessMsg
  for project, s := range t.ListHTag {
    app.params.Projects = append(
      app.params.Projects,
      &t.ProjectParams{
        Project:    project,
        IsActive:   true,
        StructType: int64(s),
        DelAfter:   3})
  }

  // створення робочого каталогу
  if _, err := os.Stat(app.params.General.WorkDir); os.IsNotExist(err) {
    if e := os.Mkdir(app.params.General.WorkDir, os.ModePerm); e != nil {
      return e
    }
  }

  // вичитування налаштувань з файлу
  _, fileName := filepath.Split(app.stnFile)

  res, err := app.readSettings()
  if err != nil {
    fmt.Printf("Помилка отримання налаштувань з \"%s\": %s\n", fileName, err)
  }

  if res {
    fmt.Printf("Налаштування отримані з \"%s\".\n", fileName)
  } else {
    fmt.Printf("Застосовані налаштування по-замовчуванню.\nЇх збережено в \"%s\".\n", fileName)
  }

  fmt.Println("-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-")
  fmt.Println()

  return nil
}

func (app *App) Run() error {
  // початкова ініціалізація
  if err := app.Init(); err != nil {
    return err
  }

  // Mongo connection
  if err := app.connectToMongo(); err != nil {
    return err
  }

  // NATS
  if err := app.connectToNATS(); err != nil {
    return err
  }

  // фонова зачистка протухших даних
  go app.runCleaner()

  // запуск прийому даних
  app.params.IsActive = true

  // запуск web-сервера
  var ws w.WebServer
  ws.Init(&app.params)
  if err := ws.Run(); err != nil {
    return err
  }

  return nil
}

func (app *App) Close() {
  // MongoDB connection closing
  if app.session != nil {
    app.session.Close()
  }

  // NATS closing
  n.Close(&app.params)
}

func (app *App) connectToMongo() error {
  session, err := m.Connect(&app.params)
  if err != nil {
    return err
  }

  session.SetMode(mgo.Monotonic, true)
  app.session = session

  return nil
}

func (app *App) connectToNATS() error {
  if app.useNats {
    if err := n.Connect(&app.params); err != nil {
      return err
    }
  }

  return nil
}

func (app *App) runCleaner() {
  m.Clean(&app.params)
}

func (app *App) readSettings() (bool, error) {
  // json -> file
  if _, err := os.Stat(app.stnFile); os.IsNotExist(err) {
    b, e := json.MarshalIndent(app.params, "", "  ")
    if e != nil {
      return false, e
    }

    // saving current params to a file
    if e = ioutil.WriteFile(app.stnFile, b, os.ModePerm); e != nil {
      return false, e
    }

    return false, nil
  } else {
    b, e := ioutil.ReadFile(app.stnFile)
    if e != nil {
      return false, e
    }

    if e = json.Unmarshal(b, &app.params); e != nil {
      return false, e
    }

    return true, nil
  }
}

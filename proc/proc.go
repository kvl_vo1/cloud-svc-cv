package proc

import (
  z "cloud-svc-cv/arch"
  t "cloud-svc-cv/types"
  u "cloud-svc-cv/utils"

  "bytes"
  "compress/gzip"
  "crypto/tls"
  "encoding/base64"
  "encoding/json"
  "errors"
  "fmt"
  "io/ioutil"
  "log"
  "net/http"
  "path/filepath"
  "strings"
)

func ProcessMsg(msg []byte, ci int, s *t.Settings) {
  if !s.IsActive {
    return
  }

  ms := s.Mongo.Session.Copy()
  defer ms.Close()

  // перевірка на валідність
  var bf t.BucketFile
  err := json.Unmarshal(msg, &bf)
  if err != nil {
    fmt.Println(err)
    return
  }

  // обробка даних
  b, err := processData(msg, ci, s)
  if err != nil {
    fmt.Println(err)
  } else {
    c := ms.DB(s.Mongo.Database).C(s.Mongo.Collections.Workfiles)

    var md t.JsonMeta
    e := json.Unmarshal(b, &md)
    if e != nil {
      log.Println(e)
    } else {
      if e = c.Insert(md); e != nil {
        fmt.Println(e)
      }
    }
  }
}

func processData(msg []byte, ci int, s *t.Settings) ([]byte, error) {
  var bf t.BucketFile

  if err := json.Unmarshal(msg, &bf); err != nil {
    return nil, u.MakeError("nats.Unmarshal", err.Error())
  }
  objFileName := bf.Object

  buf := bytes.NewReader(msg)

  req, err := http.NewRequest("POST", s.Countries[ci].PopData, buf)
  if err != nil {
    return nil, u.MakeError("http.NewRequest", err.Error()+" "+objFileName)
  }
  req.Header.Set("Content-Type", "application/json; charset=utf-8")
  req.Header.Set("Authorization", fmt.Sprintf("Basic %s", u.Base64Encode(fmt.Sprintf(t.HEADER_API_KEY, "sysuser"))))

  tr := &http.Transport{
    TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
  }
  client := &http.Client{Transport: tr}

  resp, err := client.Do(req)
  if err != nil {
    return nil, u.MakeError("client.Do", err.Error()+" "+objFileName)
  }
  defer func() {
    _ = resp.Body.Close()
  }()

  if resp.StatusCode != http.StatusOK {
    var message string
    body, e := ioutil.ReadAll(resp.Body)
    if e != nil {
      body = []byte("resp.Status/ioutil.ReadAll" + e.Error())
    }

    message = fmt.Sprintf("%d %s %s", resp.StatusCode, string(body), objFileName)
    return nil, u.MakeError("http сервера", message)
  }

  meta := resp.Header.Get("Content-Meta")
  body, err := ioutil.ReadAll(resp.Body)
  if err != nil {
    return nil, u.MakeError("ReadAll(resp.Body)", err.Error()+" "+objFileName)
  }

  var fileName = fmt.Sprintf("%s.gzip", objFileName)
  var metaBytes []byte
  if len(meta) > 0 {
    metaBytes, err = base64.StdEncoding.DecodeString(meta)
    if err != nil {
      return nil, u.MakeError("DecodeString", err.Error())
    }

    var jm t.JsonMeta
    err = json.Unmarshal(metaBytes, &jm)
    if err != nil {
      return nil, u.MakeError("meta.Unmarshal", err.Error())
    }
    // Імя готового файла для того, щоб забирати його з файлової системи
    jm.MWS.FileName = fileName

    if strings.HasPrefix(jm.CTag, "converted from") {
      jm.CTag = ""
    }

    metaBytes, err = json.Marshal(jm)
    if err != nil {
      return nil, u.MakeError("meta.Marshal", err.Error())
    }
  } else {
    return nil, u.MakeError("", "len(meta) = 0")
  }

  var bodyBytes []byte
  if len(body) > 0 {
    // імпорт архівних даних в обєкт
    b := bytes.NewReader(body)
    fz, e := gzip.NewReader(b)
    if e != nil {
      fmt.Println(e)
    }
    defer func() {
      _ = fz.Close()
    }()

    // unzip в s
    bodyBytes, e = ioutil.ReadAll(fz)
    if e != nil {
      // fmt.Println(e)
    }
  } else {
    return nil, u.MakeError("", "len(body) = 0")
  }

  wj, err := makeSingleJSON(&metaBytes, &bodyBytes, s)
  if err != nil {
    return nil, u.MakeError("MakeSingleJSON", err.Error())
  }

  fnz := filepath.Join(s.General.WorkDir, fileName)
  err = z.GzipAndSaveToFile(fnz, wj)
  if err != nil {
    return nil, u.MakeError("GzipAndSaveToFile", err.Error())
  }

  return metaBytes, nil
}

func makeSingleJSON(m, d *[]byte, s *t.Settings) ([]byte, error) {
  var jm t.JsonMeta
  err := json.Unmarshal(*m, &jm)
  if err != nil {
    return nil, errors.New(fmt.Sprintf("meta.Unmarshal -> %s", err.Error()))
  }

  if s.IsStructType(jm.HTag, int64(t.DATA_P1)) {
    var jf t.JsonPriceP1
    jf.Meta = jm
    err = json.Unmarshal(*d, &jf.Data)
    if err != nil {
      return nil, errors.New(fmt.Sprintf("data.Unmarshal -> %s", err.Error()))
    }

    jf.Meta.MWS.Items.Total = len(jf.Data)
    *m, _ = json.Marshal(jf.Meta)

    jd, e := json.Marshal(&jf)
    if e != nil {
      return nil, errors.New(fmt.Sprintf("whole json.Marshal -> %s", e.Error()))
    }

    return jd, nil
  } else
  if s.IsStructType(jm.HTag, int64(t.DATA_P3)) {
    var jf t.JsonPriceP3
    jf.Meta = jm
    err = json.Unmarshal(*d, &jf.Data)
    if err != nil {
      return nil, errors.New(fmt.Sprintf("data.Unmarshal -> %s", err.Error()))
    }

    jf.Meta.MWS.Items.Total = len(jf.Data)
    *m, _ = json.Marshal(jf.Meta)

    jd, e := json.Marshal(&jf)
    if e != nil {
      return nil, errors.New(fmt.Sprintf("whole json.Marshal -> %s", e.Error()))
    }

    return jd, nil
  } else
  if s.IsStructType(jm.HTag, int64(t.DATA_P2)) {
    var jf t.JsonPriceP2
    jf.Meta = jm
    err = json.Unmarshal(*d, &jf.Data)
    if err != nil {
      return nil, errors.New(fmt.Sprintf("data.Unmarshal -> %s", err.Error()))
    }

    jf.Meta.MWS.Items.Total = len(jf.Data)
    *m, _ = json.Marshal(jf.Meta)

    jd, e := json.Marshal(&jf)
    if e != nil {
      return nil, errors.New(fmt.Sprintf("whole json.Marshal -> %s", e.Error()))
    }

    return jd, nil
  } else {
    return nil, errors.New(fmt.Sprintf(`HTag "%s" is unknown!`, jm.HTag))
  }
}

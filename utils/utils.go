package utils

import (
  "encoding/base64"
  "errors"
  "fmt"
)

func MakeError(MsgTxt, e string) error {
  return errors.New(fmt.Sprintf("Помилка %s: %s", MsgTxt, e))
}

func Base64Encode(s string) string {
  return base64.StdEncoding.EncodeToString([]byte(s))
}

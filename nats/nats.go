package nats

import (
  t "cloud-svc-cv/types"

  "crypto/tls"
  "errors"
  "fmt"

  "github.com/nats-io/nats.go"
)

func Connect(s *t.Settings) error {
  for countryIndex, country := range s.Countries {
    if country.IsActive {
      tc := tls.Config{InsecureSkipVerify: true}

      opts := nats.GetDefaultOptions()
      opts.Servers = []string{country.NatsAddr}
      opts.Secure = true
      opts.TLSConfig = &tc
      opts.MaxReconnect = -1

      nc, err := opts.Connect()
      if err != nil {
        return errors.New(fmt.Sprintf("nats [%s] Помилка з'єднання \"%s\"", country.Country, err))
      }
      country.NC = nc

      c := countryIndex
      // підписка
      country.Subs, err = nc.Subscribe(
        country.SubsToken,
        func(m *nats.Msg) {
          go s.OnNATSData(m.Data, c, s)
        })

      if err != nil {
        return errors.New(fmt.Sprintf("nats [%s] Помилка Subscribe: \"%s\"", country.Country, err))
      }
    }
  }

  return nil
}

func Close(s *t.Settings) {
  for _, country := range s.Countries {
    if country.NC != nil {
      if country.Subs != nil {
        _ = country.Subs.Unsubscribe()
      }
      country.NC.Close()
    }
  }
}

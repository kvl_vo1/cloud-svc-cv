module cloud-svc-cv

go 1.14

require (
	github.com/nats-io/nats.go v1.10.0
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22
)

import "./css/styles.scss";

window.onload = () => {
  const GZIP = "gzip";
  const UNGZIP = "ungzip";

  const MSG_SUCC = 0;
  const MSG_FAIL = 1;
  const MSG_INFO = 2;

  const bc = document.getElementById("btn_compress");
  const bd = document.getElementById("btn_decompress");
  const srcText = document.getElementById("src_text");
  const trgText = document.getElementById("trg_text");

  const msgText = document.getElementById("msg_text");
  const icon = msgText.querySelector("#icon");
  const text = msgText.querySelector("#text");

  const url = "tools/compression"; /*"https://localhost:9000/tools/compression/";*/

  function showMessage(msgType, msgTxt) {
    msgText.style.display = "none";
    msgText.classList.remove("alert-info");
    msgText.classList.remove("alert-success");
    msgText.classList.remove("alert-danger");

    icon.classList.remove("glyphicon-info-sign");
    icon.classList.remove("glyphicon-ok-sign");
    icon.classList.remove("glyphicon-exclamation-sign");

    text.innerText = "";

    const msg = msgTxt === undefined ? "Processing..." : msgTxt;

    switch (msgType) {
    case MSG_INFO:
      icon.classList.add("glyphicon-info-sign");
      msgText.classList.add("alert-info");
      text.innerHTML = msg;
      break;

    case MSG_SUCC:
      icon.classList.add("glyphicon-ok-sign");
      msgText.classList.add("alert-success");
      text.innerHTML = `Original: "${msg.result.original || 0}" Zipped: "${msg.result.zipped || 0}" Base64: "${msg.result.base64 || 0}"`;
      break;

    case MSG_FAIL:
      icon.classList.add("glyphicon-exclamation-sign");
      msgText.classList.add("alert-danger");
      text.innerHTML = msg;
      break;

    default:
      icon.classList.add("glyphicon-info-sign");
      msgText.classList.add("alert-info");
      text.innerHTML = msg;
      break;
    }

    msgText.style.display = "block";
  }

  function success(result) {
    switch (result.operation) {
    case GZIP:
      trgText.value = result.data;
      showMessage(MSG_SUCC, result);
      break;

    case UNGZIP:
      srcText.value = result.data;
      showMessage(MSG_SUCC, result);
      break;

    default:
      showMessage(MSG_FAIL, `Operation "${result.operation}" is not supported!`);
    }
  }

  function error(XHR) {
    showMessage(MSG_FAIL, `Status:"${XHR.status}" StatusText:"${XHR.statusText}" ResponseText:"${XHR.responseText}"`);
  }

  const ajax = {
    url,
    type: "POST",
    headers: { "Content-Type": "application/json;charset=utf-8", Accept: "application/json" },
    crossDomain: true,
    cache: false,
    global: false,
    success,
    error,
    dataType: "json",
  };

  const onclick = (ev) => {
    switch (ev.currentTarget.id) {
    case "btn_compress": {
      ev.stopPropagation();

      showMessage(MSG_INFO);

      const data = JSON.stringify({
        operation: GZIP,
        data: srcText.value,
      });

      trgText.value = "";
      $.ajax(Object.assign(ajax, { data }));
      break;
    }

    case "btn_decompress": {
      ev.stopPropagation();

      showMessage(MSG_INFO);

      const data = JSON.stringify({
        operation: UNGZIP,
        data: trgText.value,
      });

      srcText.value = "";
      $.ajax(Object.assign(ajax, { data }));
      break;
    }

    default:
      ev.stopPropagation();
      showMessage(MSG_FAIL, `Target with id "${ev.target.id}" is unknown!`);
    }
  };

  bc.addEventListener("click", onclick);
  bd.addEventListener("click", onclick);
};

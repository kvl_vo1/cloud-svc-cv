package mongo

import (
  t "cloud-svc-cv/types"
  u "cloud-svc-cv/utils"

  "log"
  "time"

  "gopkg.in/mgo.v2"
  "gopkg.in/mgo.v2/bson"
)

func Connect(s *t.Settings) (*mgo.Session, error) {
  i := mgo.DialInfo{
    Addrs:    []string{s.Mongo.Host},
    Database: s.Mongo.Database,
    Username: s.Mongo.Login,
    Password: s.Mongo.Password}

  session, err := mgo.DialWithInfo(&i)
  if err != nil {
    return nil, err
  }
  s.Mongo.Session = session
  return session, nil
}

func Clean(s *t.Settings) {
  ms := s.Mongo.Session.Copy()
  defer ms.Close()

  // history collection
  c := ms.DB(s.Mongo.Database).C(s.Mongo.Collections.History)

  timer := time.NewTimer(time.Second)
  for true {
    t0 := time.Now()
    t1 := t0.Add(time.Hour * 24) // +1 день
    t1 = time.Date(t1.Year(), t1.Month(), t1.Day(), 1, 23, 45, 0, time.Local) // задаю час запуску 1:23:45 :)))
    timer.Reset(t1.Sub(t0))
    <-timer.C // жду

    log.Println("Removing expired data...")
    for _, project := range s.Projects {
      if project.IsActive {
        log.Printf("Project: '%s'. Removing...", project.Project)

        // час -Params.Projects[i].DelAfter дні назад
        tp := t1.Add(-time.Hour * 24 * time.Duration(project.DelAfter))

        q := bson.M{
          "htag": project.Project,
          "unix": bson.M{
            "$lte": tp.Unix(),
          },
        }

        info, err := c.RemoveAll(q)
        if err != nil {
          log.Println(err)
        } else {
          log.Printf("Project: '%s'. Removed %d position(s).", project.Project, info.Removed)
        }
      }
    }
  }
}

func MoveDocument(s *mgo.Session, db, srcCol, dstCol string, id bson.ObjectId) error {
  var docsList []interface{}
  wf := s.DB(db).C(srcCol)
  hf := s.DB(db).C(dstCol)

  // пошук документа
  if err := wf.Find(bson.M{"_id": id}).All(&docsList); err != nil {
    return u.MakeError("MoveDocument:wf.Find", err.Error())
  }

  // вставка в історичну колекцію
  if err := hf.Insert(docsList[0]); err != nil {
    return u.MakeError("MoveDocument:hd.Insert", err.Error())
  }

  // видалення документа з оперативної колекції
  if err := wf.RemoveId(id); err != nil {
    e := u.MakeError("MoveDocument:wf.RemoveId", err.Error())
    log.Println(e)
    return e
  }

  return nil
}

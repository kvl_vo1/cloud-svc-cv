package types

import (
  "github.com/nats-io/nats.go"
  "gopkg.in/mgo.v2"
  "gopkg.in/mgo.v2/bson"
)

const (
  HEADER_API_KEY = "api:key-%s"

  DATA_P1 = iota // [1] структура даних для project#1
  DATA_P2        // [2] структура даних для project#2
  DATA_P3        // [3] структура даних для project#3
)

var (
  ListHTag = map[string]int{
    "project#1": DATA_P1,
    "project#2": DATA_P1,
    "project#3": DATA_P3,
  }
)

type Collections struct {
  Workfiles, History, QV string
}

type onNATSData func(msg []byte, ci int, s *Settings)

type CountryParams struct {
  Country   string             `json:"Country"`
  IsActive  bool               `json:"IsActive"`
  NatsAddr  string             `json:"NatsAddr"`
  PopData   string             `json:"PopData"`
  SubsToken string             `json:"SubsToken"`
  NC        *nats.Conn         `json:"-"`
  Subs      *nats.Subscription `json:"-"`
}

type ProjectParams struct {
  Project    string `json:"Project"`
  StructType int64  `json:"StructType"`
  IsActive   bool   `json:"IsActive"`
  DelAfter   int64  `json:"DelAfter"`
}

type Settings struct {
  IsActive bool `json:"-"`
  General  struct {
    CollectGSdata bool   `json:"CollectGSdata"`
    WorkDir       string `json:"-"`
  } `json:"General,omitempty"`
  Mongo struct {
    Session     *mgo.Session `json:"-"`
    Collections Collections  `json:"-"`
    Database    string       `json:"DB"`
    Host        string       `json:"Host"`
    Login       string       `json:"Login"`
    Password    string       `json:"Password"`
  } `json:"Mongo,omitempty"`
  Countries  []*CountryParams `json:"Countries,omitempty"`
  Projects   []*ProjectParams `json:"Projects,omitempty"`
  OnNATSData onNATSData       `json:"-"`
}

func (s *Settings) IsStructType(checkProject string, checkStruct int64) bool {
  for _, p := range s.Projects {
    if (p.Project == checkProject) && (p.StructType == checkStruct) && (p.IsActive) {
      return true
    }
  }

  return false
}

type BucketFile struct {
  Bucket string `json:"bucket"`
  Object string `json:"object"`
}

type linkDrug struct {
  ID     string `json:"id,omitempty"      bson:"id,omitempty"`
  IDLink int64  `json:"id_link,omitempty" bson:"id_link,omitempty"`
  IDDrug int64  `json:"id_drug,omitempty" bson:"id_drug,omitempty"`
  IDBrnd int64  `json:"id_brnd,omitempty" bson:"id_brnd,omitempty"`
  IDCatg int64  `json:"id_catg,omitempty" bson:"id_catg,omitempty"`
  IDStat int64  `json:"id_stat,omitempty" bson:"id_stat,omitempty"`
}

type linkAddr struct {
  ID     string `json:"id,omitempty"      bson:"id,omitempty"`
  IDLink int64  `json:"id_link,omitempty" bson:"id_link,omitempty"`
  IDAddr int64  `json:"id_addr,omitempty" bson:"id_addr,omitempty"`
  IDOrgn int64  `json:"id_orgn,omitempty" bson:"id_orgn,omitempty"`
  IDStat int64  `json:"id_stat,omitempty" bson:"id_stat,omitempty"`
  EGRPOU string `json:"egrpou,omitempty"  bson:"egrpou,omitempty"`
}

type linkAuth struct {
  ID   string `json:"id,omitempty"   bson:"id,omitempty"`
  Name string `json:"name,omitempty" bson:"name,omitempty"`
}

type FileItem struct {
  ID       bson.ObjectId `json:"id" bson:"_id"`
  FileName string        `json:"fileName,omitempty" bson:"fileName,omitempty"`
  Mws      *struct {
    Filename string `json:"-" bson:"fileName,omitempty"`
  } `json:"-" bson:"mws,omitempty"`
}

type JsonMeta struct {
  UUID string   `json:"uuid,omitempty" bson:"uuid,omitempty"`
  Auth linkAuth `json:"auth,omitempty" bson:"auth,omitempty"`
  Host string   `json:"host,omitempty" bson:"host,omitempty"`
  User string   `json:"user,omitempty" bson:"user,omitempty"`

  Unix int64 `json:"unix,omitempty" bson:"unix,omitempty"`

  HTag string   `json:"htag,omitempty" bson:"htag,omitempty"`
  Span []string `json:"span,omitempty" bson:"span,omitempty"`
  Nick string   `json:"nick,omitempty" bson:"nick,omitempty"`

  Name string `json:"name,omitempty" bson:"name,omitempty"`
  Head string `json:"head,omitempty" bson:"head,omitempty"`
  Addr string `json:"addr,omitempty" bson:"addr,omitempty"`
  Code string `json:"code,omitempty" bson:"code,omitempty"`

  Link linkAddr `json:"link,omitempty" bson:"link,omitempty"`
  Frwd string   `json:"frwd,omitempty" bson:"frwd,omitempty"`
  CTag string   `json:"ctag,omitempty" bson:"ctag,omitempty"`
  ETag string   `json:"etag,omitempty" bson:"etag,omitempty"`
  Test bool     `json:"test,omitempty" bson:"test,omitempty"`

  MWS `json:"mws,omitempty" bson:"mws,omitempty"`
}

type QVLog struct {
  ID         bson.ObjectId `json:"id" bson:"_id"`
  Unix       int64         `json:"unix,omitempty" bson:"unix,omitempty"`
  ReceivedTS string        `json:"receivedTS,omitempty" bson:"receivedTS,omitempty"`
  HTag       string        `json:"htag,omitempty" bson:"htag,omitempty"`
  Auth       linkAuth      `json:"auth,omitempty" bson:"auth,omitempty"`
  Link       struct {
    ID      string `json:"id,omitempty"      bson:"id,omitempty"`
    IDLink  int64  `json:"id_link,omitempty" bson:"id_link,omitempty"`
    IDAddr  int64  `json:"id_addr,omitempty" bson:"id_addr,omitempty"`
    IDOrgn  int64  `json:"id_orgn,omitempty" bson:"id_orgn,omitempty"`
    EGRPOU  string `json:"egrpou,omitempty"  bson:"egrpou,omitempty"`
    OrgName string `json:"org_name,omitempty"  bson:"org_name,omitempty"`
  } `json:"link,omitempty" bson:"link,omitempty"`
  MWS `json:"mws,omitempty" bson:"mws,omitempty"`
}

type MWS struct {
  ID       bson.ObjectId `json:"id,omitempty" bson:"_id,omitempty"`
  HTag     string        `json:"htag,omitempty" bson:"-"`
  FileName string        `json:"fileName,omitempty" bson:"fileName,omitempty"`

  ReceiveID int    `json:"receiveID,omitempty" bson:"receiveID,omitempty"`
  BrNick    string `json:"brNick,omitempty" bson:"brNick,omitempty"`
  OrgId     int    `json:"orgId,omitempty" bson:"orgId,omitempty"`
  OrgName   string `json:"orgName,omitempty" bson:"orgName,omitempty"`
  CorpId    int    `json:"corpId,omitempty" bson:"corpId,omitempty"`
  CorpName  string `json:"corpName,omitempty" bson:"corpName,omitempty"`
  Items     struct {
    Total    int `json:"total,omitempty" bson:"total,omitempty"`
    New      int `json:"new,omitempty" bson:"new,omitempty"`
    Unlinked int `json:"unlinked,omitempty" bson:"unlinked,omitempty"`
  } `json:"items,omitempty" bson:"items,omitempty"`
  Message      string `json:"message,omitempty" bson:"message,omitempty"`
  IsWarning    bool   `json:"isWarning,omitempty" bson:"isWarning,omitempty"`
  IsError      bool   `json:"isError,omitempty" bson:"isError,omitempty"`
  LastUpdate   int64  `json:"lastUpdate,omitempty" bson:"lastUpdate,omitempty"`
  LastUpdateTS string `json:"lastUpdateTS,omitempty" bson:"lastUpdateTS,omitempty"`
}

type itemV3P1 struct {
  ID    string   `json:"id,omitempty"`
  Name  string   `json:"name,omitempty"`
  Home  string   `json:"home,omitempty"`
  Quant float64  `json:"quant,omitempty"`
  Price float64  `json:"price,omitempty"`
  Link  linkDrug `json:"link,omitempty"`
}

type itemV3P2 struct {
  ID        string   `json:"id,omitempty"`
  Name      string   `json:"name,omitempty"`
  QuantIn   float64  `json:"quant_in,omitempty"`
  PriceIn   float64  `json:"price_in,omitempty"`
  QuantOut  float64  `json:"quant_out,omitempty"`
  PriceOut  float64  `json:"price_out,omitempty"`
  Stock     float64  `json:"stock,omitempty"`
  Reimburse bool     `json:"reimburse,omitempty"`
  SuppName  string   `json:"supp_name,omitempty"`
  SuppCode  string   `json:"supp_code,omitempty"`
  LinkAddr  linkAddr `json:"link_addr,omitempty"`
  LinkDrug  linkDrug `json:"link_drug,omitempty"`
}

type itemV3P3 struct {
  ID       string   `json:"id,omitempty"`
  Name     string   `json:"name,omitempty"`
  QuantIn  float64  `json:"quant_in,omitempty"`
  PriceIn  float64  `json:"price_in,omitempty"`
  QuantOut float64  `json:"quant_out,omitempty"`
  PriceOut float64  `json:"price_out,omitempty"`
  PriceRoc float64  `json:"price_roc,omitempty"`
  Stock    float64  `json:"stock,omitempty"`
  StockTab float64  `json:"stock_tab,omitempty"`
  Link     linkDrug `json:"link,omitempty"`
}

type JsonPriceP1 struct {
  Meta JsonMeta   `json:"meta"`
  Data []itemV3P1 `json:"data"`
}

type JsonPriceP2 struct {
  Meta JsonMeta   `json:"meta"`
  Data []itemV3P2 `json:"data"`
}

type JsonPriceP3 struct {
  Meta JsonMeta   `json:"meta"`
  Data []itemV3P3 `json:"data"`
}
